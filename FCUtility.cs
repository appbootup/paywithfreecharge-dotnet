using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;

namespace PayWithFC
{
    public class FCUtility
    {
        public static String Encrypt(String accessToken, String secretKey)
        {
            var plainBytes = Encoding.UTF8.GetBytes(accessToken);
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));

            RijndaelManaged rijndaelManaged = new RijndaelManaged
            {
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128,
                Key = keyBytes
            };
            
            byte[] crypt = rijndaelManaged.CreateEncryptor().TransformFinalBlock(plainBytes, 0, plainBytes.Length);
            return GetHexString(crypt);
            
        }
        
	public static string Checksum(object obj, String secretKey) {
	    var settings = new JsonSerializerSettings() {
                ContractResolver = new OrderedContractResolver()
            };

            var json = JsonConvert.SerializeObject(obj, Formatting.None, settings);
			string s = json + secretKey;
            Console.WriteLine(s);
			System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
			byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(s), 0, Encoding.UTF8.GetByteCount(s));
            return GetHexString(crypto);

	}
        
        public static string GetHexString(byte[] bytes) {
            
            System.Text.StringBuilder str = new System.Text.StringBuilder();
            foreach (byte theByte in bytes) {
				str.Append(theByte.ToString("x2"));
			}
			return str.ToString();
        }
        
        public static void Main(string[] args)
        {
            string key = "xxxxxx-merchant-key-xxxxxxxxxx";
            
            MerchantPaymentRequest request = new MerchantPaymentRequest();
            request.merchantTxnId = "Test0003";
            request.amount = "1.00";
            request.furl = "http://localhost/production/c7ts/app/freeChargeR.aspx";
            request.surl = "http://localhost/production/c7ts/app/freeChargeR.aspx";
            request.productInfo = "auth 123";
            request.email = "test@c7apps.com";
            request.mobile = "XXXXXXXXXX";
            request.channel = "WEB";
            request.merchantId = "XXXXXXXXXXXXXX";
            request.customerName = "abx";
            
            Console.WriteLine(FCUtility.Checksum(request,key));
            Console.WriteLine(FCUtility.Encrypt("qwerty",key));
        }
    }
    
    public class OrderedContractResolver : DefaultContractResolver
    {
        protected override System.Collections.Generic.IList<JsonProperty> CreateProperties(System.Type type, MemberSerialization memberSerialization) {
            return base.CreateProperties(type, memberSerialization).OrderBy(p => p.PropertyName).ToList();
        }
    }
    
    public class MerchantPaymentRequest{
        public string merchantTxnId{get;set;}
        public string amount{get;set;}
        public string furl{get;set;}
        public string surl{get;set;}
        public string productInfo{get;set;}
        public string email{get;set;}
        public string mobile{get;set;}
        public string channel{get;set;}
        public string merchantId{get;set;}
        public string customerName{get;set;}
    }
}
